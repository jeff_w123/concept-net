const assert = require('assert'),
    sinon = require('sinon'),
    app = require('./app.js');

let getParentNodesStub;

describe('Test app function buildJson():', () => {

    // tear down
    afterEach(() => {

        getParentNodesStub.restore();
    });

    it('should call getParentNodes() correct number of times', (itCallback) => {
        // Arrange
        const ITERATIONS = 10;
        const stubResult = [{ "some_property": {} }];
        getParentNodesStub = sinon.stub(app, 'getParentNodes').yields(null, stubResult);

        // Act
        app.buildJson('carriage', ITERATIONS, (err, builtJson) => {

            // Assert
            assert.equal(getParentNodesStub.callCount, ITERATIONS);
            itCallback();
        });
    });

    it('should use last iteration result from getParentNodes()', (itCallback) => {
        // Arrange
        const stubWrongResult = [{ "wrong_property": {} }];
        const stubExpectedResult = [{ "expected_property": {} }];
        getParentNodesStub = sinon.stub(app, 'getParentNodes');
        getParentNodesStub.yields(null, stubWrongResult);
        getParentNodesStub.onCall(2).yields(null, stubExpectedResult);


        // Act
        app.buildJson('train', 3, (err, builtJson) => {
            // Assert
            const EXPECTED_RES = { "expected_property": {} };
            assert.equal(builtJson, JSON.stringify(EXPECTED_RES, null, 10));
            itCallback();
        });
    });

    it('should build json correctly', (itCallback) => {
        // Arrange
        const stubResult = [{ "propX": { "propXa": {}} }, { "propX": { "propXb": { "propY": {}}} }];
        getParentNodesStub = sinon.stub(app, 'getParentNodes').yields(null, stubResult);

        // Act
        app.buildJson('aeroplane', 2, (err, builtJson) => {

            // Assert
            const EXPECTED_RES = { "propX": { "propXa": {}, "propXb": { "propY": {} }}};
            assert.equal(builtJson, JSON.stringify(EXPECTED_RES, null, 10));
            itCallback();
        });
    });
});

describe('Verify call to external api:', () => {

    it('should not return error.', (itCallback) => {
        // Arrange
        const START_NODE = { "supreme_court": {}};

        // Act
        app.doNetworkCall(START_NODE, (err, result) => {

            // Assert
            assert.equal(err, null);
            itCallback();
        });
    });
});

const async = require('async'),
    request = require('request'),
    _ = require('lodash');

// input
const SAMPLE_INPUT = 'supreme court';

// api throttling options
const GRAPH_DEPTH_MAX = 2;
const API_PAGE_SIZE = 3;

// config
const URL_PREFIX = `http://api.conceptnet.io/query?start=/c/en/`;
const URL_SUFFIX = `&rel=/r/IsA&offset=0&limit=${API_PAGE_SIZE}`;

const app = {

    run: () => {
        console.log(`<<<<<<< Input is....${SAMPLE_INPUT} <<<<<<<`);
        app.buildJson(SAMPLE_INPUT, GRAPH_DEPTH_MAX, (err, builtJson) => {
            if (err) {
                console.log('*****', err);
                return;
            }
            console.log('>>>>>>>', builtJson);
        })
    },

    buildJson: (sampleInput, graphDepthMax, buildCb) => {
        if (!sampleInput) return buildCb(new Error('An input string is required.'));
        sampleInput = sampleInput.replace(' ', '_');
        const initNode = {};
        initNode[sampleInput] = {};

        let finalResult = {};
        let graphDepth = 0;
        let nodesToProcess = [initNode];

        async.whilst(
            apiThrottleCheck = (throttleCb) => { throttleCb(null, graphDepth < graphDepthMax); },

            async.retryable({ times: 3, interval: 500 },
                doIterationsAsync = (iterationCb) => {
                    graphDepth++;
                    app.getParentNodes(nodesToProcess, (err, foundNodes) => {
                        if (err) return iterationCb(err);
                        if (!foundNodes[0]) return iterationCb(new Error('Search did not return any results.'));
                        nodesToProcess = foundNodes;

                        // callback happens when throttle is reached
                        iterationCb(null, foundNodes)
                    });
                }),
            (err, nodeTreeBranches) => {
                if (err) return buildCb(err);

                nodeTreeBranches.map((branch) =>{
                    _.merge(finalResult,branch)
                });
                buildCb(null, JSON.stringify(finalResult, null, 10));
            }
        );
    },

    getParentNodes: (nodes, cb) => {

        const parallelTasks = [];
        nodes.forEach((ele) => {
            parallelTasks.push((paraCb) => {
                app.doNetworkCall(ele, (err, networkResponse) => {
                    if (err) return paraCb(err);
                    const links = networkResponse.edges.map((edge) => {
                        let newNode = {};
                        newNode[edge.end.term.replace('/c/en/','')] = ele;
                        return newNode;
                    });
                    paraCb(null, links);
                });
            });
        });

        async.parallelLimit(parallelTasks, 5, (err, paraResults) => {
            if (err) return cb(err);
            cb(null, _.flatten(paraResults));
        });
    },

    doNetworkCall: (startNode, networkCallback) => {
        const SEARCH_TERM = Object.keys(startNode)[0];

        if (!SEARCH_TERM || SEARCH_TERM === '0')
            return networkCallback(new Error(`Could not get search term from ${startNode}`));

        const options = { method: 'GET', url: `${URL_PREFIX}${SEARCH_TERM}${URL_SUFFIX}` };

        request(options, (error, response, body) => {
            if (error) return networkCallback(error);

            if (response.statusCode && response.statusCode === 200) {
                let result;
                try {
                    result = JSON.parse(body);
                } catch (e) {
                    return networkCallback(new Error(`Could not parse response body, exception is: ${e}`));
                }
                return networkCallback(null, result);
            } else {
                return networkCallback(new Error(`Request to ${URL} did not return a valid response.`));
            }
        });
    },
};

module.exports = app;

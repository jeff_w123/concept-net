Hi There

Thank you for taking the time to review my code.

I look forward to your feedback.

Jeff


********* INSTALL AND RUN *********

Machine will require Nodejs installed.
Internet access required for calls to ConceptNet api.

// install dependencies
npm install

// run app
node run

// run tests
mocha tests

********* TASK NOTES: *********

I used Lodash and Async libraries. I am familiar with these.

I used Sinon for test stubbing.

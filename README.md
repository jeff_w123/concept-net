## A NodeJs app to display the parent concepts of a search term
<h3>View the app requirements: </h3> 
<a href = "https://gitlab.com/jeff_w123/concept-net/-/blob/master/App%20Building%20Code%20Test.pdf">App Building Code Test.pdf</a>
<h3>Requirements:</h3>
- NodeJs
<br>- Internet connection to access the ConceptNet api

<h3>Install dependencies:</h3>
- npm install

<h3>Run app:</h3>
- node run

<h3>Run tests:</h3>
- mocha tests
